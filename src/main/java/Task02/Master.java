package Task02;

import java.util.Comparator;
import java.util.List;

/** class which describe a houseowner */
public class Master {
	/** field - houseowners name */
	private String name;
	/** field - houseowners age */
	private int age;
	/** list of all electrical appliances in the house	 */
	private List<ElectricalAppliance>list;
	/** the power of all enabled appliances */
	private int result;

	/**
	 * constructor - create a new instance
	 * @param name
	 * @param age
	 */
	Master(String name, int age){
		this.name = name;
		this.age = age;
	}

	/**
	 * funсtion which set the homeowners name
	 * @param name
	 */
	public void setName(String name){
		this.name = name;
	}

	/**
	 * funсtion which return the homeowners name
	 * @return name
	 */
	public String getName(){
		return name;
	}

	/**
	 * funсtion which set the homeowners age
	 * @param age
	 */
	public void setAge(int age){
		this.age = age;
	}

	/**
	 * funсtion which return the homeowners age
	 * @return age
	 */
	public int getAge(){
		return age;
	}

	/**
	 * funсtion which turn on elecrical appliance
	 * @param appliance - elecrical appliance
	 */
	public void turnOnElecricalAppliances(ElectricalAppliance appliance ){
	appliance.setIsWorking(true);
	}

	/**
	 * function which set the electrical appliances list
	 * @param list
	 */
     public void setList(List<ElectricalAppliance>list){
		this.list = list;
     }

	/**
	 * funсtion which sort elecrical appliances by their power
	 * @param list - list of elecrical appliances
	 */
	public void sortByPower(List<ElectricalAppliance>list){
		this.list = list;

	     list.sort(Comparator.comparing(ElectricalAppliance::getPower));

	     for (ElectricalAppliance f : list) {
		     System.out.println(f.getName());
	     }
     }

	/**
	 * function which show the power of all enabled appliances
	 * @param list - list of elecrical appliances
	 */
	public void takeSummOfPower(List<ElectricalAppliance>list){
		result =0;
		for(ElectricalAppliance appliance :  list){
			if(appliance.getIsWork()==true) result+=appliance.getPower();
		}
	     System.out.println("Power of all working appliances = "+ result);
     }
}






