package Task02;

/**
 *Parent class for all appliances types
 * @author Stepan Synoviat
 * @version 1.0
 */
public class ElectricalAppliance {
	/** field name of Electrical appliance	 */
	private String name;
	/** field model of Electrical appliance	 */
	private String model;
	/**field color of Electrical appliance	 */
	private String color;
	/** field power of Electrical appliance	 */
	private int power;
	/** field which show,does appliance is working	 */
	private boolean isWorking;

	/**
	 * konstractor - create a new instance with parametrs
	 * @param name
	 * @param power
	 */
	ElectricalAppliance(String name, int power){
		this.name = name;
		this.power = power;
	}

	/**
	 * funсtion which set the name of appliance
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * funсtion which return the name of appliance
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * funсtion which set the model of appliance
	 * @param model
	 */
	public void setModel(String model) {
		this.model = model;
	}

	/**
	 * funсtion which return the model of appliance
	 * @return model
	 */
	public String getModel() {
		return model;
	}

	/**
	 * funсtion which set the color of appliance
	 * @param color
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * funсtion which return the color of appliance
	 * @return color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * funсtion which set the power of appliance
	 * @param power
	 */
	public void setPower(int power) {
		this.power = power;
	}

	/**
	 * funсtion which return the power of appliance
	 * @return power
	 */
	public int getPower() {
		return power;
	}

	/**
	 * funсtion which set the status "isWorking" for appliance
	 * @param isWorking
	 */
	public void setIsWorking(boolean isWorking){
		this.isWorking = isWorking;
	}

	/**
	 * funсtion which return the "status" of appliance
	 * @return isWorking
	 */
	public boolean getIsWork(){
		return isWorking;
	}

}
