package Task02;

public class AppliancesForCalculation extends ElectricalAppliance implements Display{


	AppliancesForCalculation(String name, int power) {
		super(name, power);
	}

	@Override
	public void show() {
		System.out.println("Show the result of electricity consumption");
	}
}
