package Task02;

public class KitchenAppliances extends ElectricalAppliance{

	KitchenAppliances(String name, int power) {
		super(name, power);
	}

	public void dealWithFood(){
		System.out.println(" I have deal with food");
	}

}
