package Task02;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		Master master = new Master("Steve", 25);

		System.out.println(master.getName());

		List<ElectricalAppliance> list = new ArrayList<>();

		KitchenAppliances a1 = new KitchenAppliances("Посудомийка", 780);

		General a2 = new General("Кондиціонер", 1200);
		master.turnOnElecricalAppliances(a1);
		master.turnOnElecricalAppliances(a2);


		list.add(0, new General("Холодильник", 1000));
		list.add(1, new AppliancesForEnjoy("TV", 700));
		list.add(2, a1);
		list.add(3, a2);

		master.sortByPower(list);
		System.out.println(" ");
		master.takeSummOfPower(list);
	}
}

